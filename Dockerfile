FROM alpine

WORKDIR /src/

COPY . .

RUN apk add dnsmasq

ENTRYPOINT [ "/usr/sbin/dnsmasq","--no-daemon" ]